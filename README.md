docker-denkmal-build
====================

Container image used to build [denkmal-website](https://gitlab.com/denkmal/denkmal-website).

The image includes Podman, NodeJS (in the specific version required), and build tools needed to build some NPM packages.


Usage
-----

The image is available as: `registry.gitlab.com/denkmal/docker-denkmal-build:TAG`.

List of tags: https://gitlab.com/denkmal/docker-denkmal-build/container_registry

Development
-----------

Build the image:
```
podman build -t docker-denkmal-build .
```

Run the image:
```
podman run docker-denkmal-build sh
```

Release
-------

The image is built and pushed to the registry by Gitlab CI.

To create a tagged release simply push a git-tag:
1. Check out desired commit
2. Create the tag: `git tag -a vX.Y.Z`
3. Push tag: `git push --tags`
