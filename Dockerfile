FROM docker.io/library/alpine:3.17.2

# Install build tools (required by some NPM packages)
RUN apk add --no-cache make g++

# Install python
RUN apk add --no-cache python3 py3-pip

# Install podman
RUN apk add --no-cache podman

# Install podman-compose
RUN pip3 install https://github.com/containers/podman-compose/archive/08ffcf6.tar.gz

# Workaround for Podman error: "overlay is not supported over overlayfs"
# See https://github.com/containers/buildah/issues/3666
RUN apk add --no-cache fuse-overlayfs
RUN sed -i 's/.*mount_program =.*/mount_program = "\/usr\/bin\/fuse-overlayfs"/' /etc/containers/storage.conf

# Install NodeJs v13
RUN cd $(mktemp -dt) &&\
  apk add --no-cache curl &&\
  curl -sL 'https://unofficial-builds.nodejs.org/download/release/v13.14.0/node-v13.14.0-linux-x64-musl.tar.gz' | tar -xzf - &&\
  cp -R node-*/bin/* /usr/local/bin/ &&\
  cp -R node-*/lib/node_modules /usr/local/lib/ &&\
  node --version && npm --version

# Install yarn
RUN npm install --global yarn@1.22.19 &&\
    yarn --version

# Cleanup
RUN rm -rf /tmp/*
